// // ============================== PASSPORT LOCAL =======================
// const passport = require("passport");
// const bcrypt = require("bcrypt");
// const LocalStrategy = require("passport-local").Strategy;
// const { Users } = require("../../models");

// passport.use(
//   new LocalStrategy(
//     { usernameField: "username", passwordField: "password" },
//     async (username, password, callback) => {
//       const user = await Users.findOne({
//         where: { username },
//       });

//       if (user) {
//         const validPass = await bcrypt.compare(password, user.password);
//         if (validPass) callback(null, user);
//         else return callback(null, false);
//       } else {
//         return callback(null, false);
//       }
//     }
//     )
//     );

//     passport.serializeUser((user, done) => done(null, user));
//     passport.deserializeUser((user, done) => done(null, user));

//     module.exports = passport;
//     // ============================== PASSPORT LOCAL =======================

const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

const { Users } = require("../../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "secret",
    },
    async (payload, done) => {
      // pengambilan data detail user
      const user = await Users.findByPk(payload.id);

      if (user) done(null, user);
      else done(null, false);
      // done(null, { name: "aku" });
    }
  )
);

module.exports = passport;

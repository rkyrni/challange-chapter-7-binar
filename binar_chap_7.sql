-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "GameRoundHistories_id_seq";

-- Table Definition
CREATE TABLE "public"."GameRoundHistories" (
    "id" int4 NOT NULL DEFAULT nextval('"GameRoundHistories_id_seq"'::regclass),
    "roundId" int4 NOT NULL,
    "playerId" int4 NOT NULL,
    "selected" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "GameRoundHistories_roundId_fkey" FOREIGN KEY ("roundId") REFERENCES "public"."GameRounds"("id"),
    CONSTRAINT "GameRoundHistories_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "GameRounds_id_seq";

-- Table Definition
CREATE TABLE "public"."GameRounds" (
    "id" int4 NOT NULL DEFAULT nextval('"GameRounds_id_seq"'::regclass),
    "roomId" int4 NOT NULL,
    "winnerId" int4,
    "status" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "GameRounds_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "public"."Rooms"("id"),
    CONSTRAINT "GameRounds_winnerId_fkey" FOREIGN KEY ("winnerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "RoomPlayers_id_seq";

-- Table Definition
CREATE TABLE "public"."RoomPlayers" (
    "id" int4 NOT NULL DEFAULT nextval('"RoomPlayers_id_seq"'::regclass),
    "roomId" int4 NOT NULL,
    "playerId" int4 NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "RoomPlayers_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "public"."Rooms"("id"),
    CONSTRAINT "RoomPlayers_playerId_fkey" FOREIGN KEY ("playerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "Rooms_id_seq";

-- Table Definition
CREATE TABLE "public"."Rooms" (
    "id" int4 NOT NULL DEFAULT nextval('"Rooms_id_seq"'::regclass),
    "code" varchar(255) NOT NULL,
    "roundNum" int4 NOT NULL,
    "createdBy" int4 NOT NULL,
    "winnerId" int4,
    "status" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    CONSTRAINT "Rooms_createdBy_fkey" FOREIGN KEY ("createdBy") REFERENCES "public"."Users"("id"),
    CONSTRAINT "Rooms_winnerId_fkey" FOREIGN KEY ("winnerId") REFERENCES "public"."Users"("id"),
    PRIMARY KEY ("id")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."SequelizeMeta" (
    "name" varchar(255) NOT NULL,
    PRIMARY KEY ("name")
);

-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "Users_id_seq";

-- Table Definition
CREATE TABLE "public"."Users" (
    "id" int4 NOT NULL DEFAULT nextval('"Users_id_seq"'::regclass),
    "email" varchar(255) NOT NULL,
    "username" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "password" varchar(255) NOT NULL,
    "role" varchar(255) NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    "deletedAt" timestamptz,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 1, 2, 'SCISSOR', '2022-09-25 01:56:42.854+07', '2022-09-25 01:56:42.854+07', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(2, 1, 1, 'ROCK', '2022-09-25 01:57:13.113+07', '2022-09-25 01:57:13.113+07', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(3, 2, 1, 'PAPER', '2022-09-25 01:57:24.984+07', '2022-09-25 01:57:24.984+07', NULL);
INSERT INTO "public"."GameRoundHistories" ("id", "roundId", "playerId", "selected", "createdAt", "updatedAt", "deletedAt") VALUES
(4, 2, 2, 'ROCK', '2022-09-25 01:57:50.531+07', '2022-09-25 01:57:50.531+07', NULL),
(5, 3, 2, 'ROCK', '2022-09-25 01:57:58.128+07', '2022-09-25 01:57:58.128+07', NULL),
(6, 3, 1, 'SCISSOR', '2022-09-25 01:58:20.426+07', '2022-09-25 01:58:20.426+07', NULL);

INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 1, 1, 'FINISHED', '2022-09-25 01:56:42.85+07', '2022-09-25 01:57:13.123+07', NULL);
INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(2, 1, 1, 'FINISHED', '2022-09-25 01:57:24.98+07', '2022-09-25 01:57:50.536+07', NULL);
INSERT INTO "public"."GameRounds" ("id", "roomId", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(3, 1, 2, 'FINISHED', '2022-09-25 01:57:58.126+07', '2022-09-25 01:58:20.432+07', NULL);

INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 1, 1, '2022-09-25 01:55:44.351+07', '2022-09-25 01:55:44.351+07', NULL);
INSERT INTO "public"."RoomPlayers" ("id", "roomId", "playerId", "createdAt", "updatedAt", "deletedAt") VALUES
(2, 1, 2, '2022-09-25 01:56:20.118+07', '2022-09-25 01:56:20.118+07', NULL);


INSERT INTO "public"."Rooms" ("id", "code", "roundNum", "createdBy", "winnerId", "status", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 'THVnUvBvi4', 3, 1, 1, 'FINISHED', '2022-09-25 01:55:44.344+07', '2022-09-25 01:58:20.444+07', NULL);


INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220915064304-create-user.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220921095705-create-rooms.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220921100825-create-room-players.js');
INSERT INTO "public"."SequelizeMeta" ("name") VALUES
('20220921101319-create-game-rounds.js'),
('20220921102116-create-game-round-histories.js');

INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(1, 'tes1@gmail.com', 'player1', 'player1', '$2b$10$J7yPUozze0J4RhVi.gLjLOU36o1q.gasOBVsOsAd7kvi4HES7/EaG', 'PLAYER', '2022-09-25 01:54:15.45+07', '2022-09-25 01:54:15.45+07', NULL);
INSERT INTO "public"."Users" ("id", "email", "username", "name", "password", "role", "createdAt", "updatedAt", "deletedAt") VALUES
(2, 'tes2@gmail.com', 'player2', 'player2', '$2b$10$AXTyqa2OiaNoh3c5UasiReKpOx0tSerUwSJ2dSdVNC02.Rq8kpvEO', 'PLAYER', '2022-09-25 01:54:28.884+07', '2022-09-25 01:54:28.884+07', NULL);


const express = require("express");
const bodyParser = require("body-parser");

const authenticationController = require("./controller/authentication");
const gameController = require("./controller/game");
const checkWinner = require("./controller/checkWinner");

const passport = require("./lib/passport");

const app = express();
const port = 8082;

app.use(bodyParser.json());
app.use(passport.initialize());

app.post("/login", authenticationController.login);

app.post("/register", authenticationController.register);

app.post(
  "/room",
  passport.authenticate("jwt", { session: false }),
  gameController.createRoom
);

app.post(
  "/join",
  passport.authenticate("jwt", { session: false }),
  gameController.joinRoom
);

app.post(
  "/play",
  passport.authenticate("jwt", { session: false }),
  gameController.play
);

app.get("/winner/:roomCode", checkWinner.checkWinner);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

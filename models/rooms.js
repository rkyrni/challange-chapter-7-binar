"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Rooms.hasMany(models.RoomPlayers, {
        foreignKey: "roomId",
      });
      models.Rooms.hasMany(models.GameRounds, {
        foreignKey: "roomId",
      });
    }
  }
  Rooms.init(
    {
      code: DataTypes.STRING,
      roundNum: DataTypes.INTEGER,
      createdBy: DataTypes.INTEGER,
      winnerId: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      status: DataTypes.STRING,
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      sequelize,
      modelName: "Rooms",
    }
  );
  return Rooms;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class GameRoundHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.GameRoundHistories.belongsTo(models.GameRounds, {
        foreignKey: "roundId",
      });
    }
  }
  GameRoundHistories.init(
    {
      roundId: DataTypes.INTEGER,
      playerId: DataTypes.INTEGER,
      selected: DataTypes.STRING,
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      sequelize,
      modelName: "GameRoundHistories",
    }
  );
  return GameRoundHistories;
};

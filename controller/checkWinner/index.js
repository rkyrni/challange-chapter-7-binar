const { Rooms, RoomPlayers, GameRounds } = require("../../models");

exports.checkWinner = async (req, res) => {
  const code = req.params.roomCode;
  const room = await Rooms.findOne({
    where: {
      code,
    },
    include: [RoomPlayers, GameRounds],
  });

  if (!room) {
    res.status(404).send("Room Code Invalid");
    return;
  }

  if (room.status === "ACTIVE") {
    res.send("Rounde belum berakhir");
    return;
  }

  res.json({
    code,
    status: room.status,
    winnerId: room.winnerId !== null ? room.winnerId : "DRAW",
    players: [room.RoomPlayers[0].playerId, room.RoomPlayers[1].playerId],
    history: [
      {
        round: 1,
        winnerId:
          room.GameRounds[0].winnerId !== null
            ? room.GameRounds[0].winnerId
            : "DRAW",
      },
      {
        round: 2,
        winnerId:
          room.GameRounds[1].winnerId !== null
            ? room.GameRounds[1].winnerId
            : "DRAW",
      },
      {
        round: 3,
        winnerId:
          room.GameRounds[2].winnerId !== null
            ? room.GameRounds[2].winnerId
            : "DRAW",
      },
    ],
  });
};

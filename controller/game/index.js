const randomCode = require("../../helper/randomCode");
const {
  Rooms,
  RoomPlayers,
  GameRounds,
  GameRoundHistories,
} = require("../../models");

const checkRoleGame = require("../../helper/roleGame");
const calculation = require("../../helper/calculation");

exports.createRoom = async (req, res) => {
  const roundNum = req.body.roundNum;
  let code = randomCode(10);
  const createdBy = req.user.id;
  const status = "ACTIVE";
  try {
    let counterValidasiRoom = true;
    while (counterValidasiRoom) {
      const validasiRoom = await Rooms.findOne({
        where: { code },
      });
      if (!validasiRoom) counterValidasiRoom = false;
      else code = randomCode(10);
    }

    const newRoom = await Rooms.create({ code, roundNum, createdBy, status });

    const payload = {
      roomId: newRoom.id,
      playerId: newRoom.createdBy,
    };

    await RoomPlayers.create(payload);
    res.json({
      code: code,
      message: "Room Berhasil dibuat",
    });
  } catch (error) {
    console.log(error);
  }
};

exports.joinRoom = async (req, res) => {
  const { code } = req.body;

  const room = await Rooms.findOne({
    where: { code },
    include: RoomPlayers,
  });

  if (!room) res.status(404).send("Room tidak ditemukan!!!");
  else {
    const find = room.RoomPlayers.find((room) => room.playerId === req.user.id);
    if (find) res.status(500).send("User sudah di dalam room!!");
    else {
      if (room.RoomPlayers.length === 2) res.status(500).send("Room penuh!!");
      else {
        const payload = {
          roomId: room.id,
          playerId: req.user.id,
        };
        await RoomPlayers.create(payload);
        res.send("Berhasil masuk room");
      }
    }
  }
};

exports.play = async (req, res) => {
  const { code, selected } = req.body;

  const isJoined = await RoomPlayers.findOne({
    where: { playerId: req.user.id },
  });

  // === Handel apakah user sudah melalukan join atau belum
  if (!isJoined) {
    res
      .status(500)
      .send(
        "Anda belum join room tersebut, silahkan join room terlebih dahulu"
      );
    return;
  }

  const room = await Rooms.findOne({
    where: { code },
    include: [RoomPlayers, GameRounds],
  });

  // === Handel saat user masuk ke room lain
  let counterCheckUserRoom = true;
  if (room) {
    room.RoomPlayers.forEach((player) => {
      if (player.playerId === req.user.id) counterCheckUserRoom = false;
    });

    if (counterCheckUserRoom) {
      res
        .status(500)
        .send("Anda belum join room ini,silahkan join atau buat room baru!!");
      return;
    }
  }

  // ==== Handel code room invalid
  if (!room) {
    res.status(404).send("Code room tidak ditemukan!");
    return;
  }

  // === Handel selected
  if (selected !== "ROCK" && selected !== "PAPER" && selected !== "SCISSOR") {
    res.status(500).send("Invalid Selected!!");
    return;
  }

  // ==== Handel apabila pemilik room melakukan play disaan belum punya lawan
  if (room.RoomPlayers.length < 2 && req.user.id === room.createdBy) {
    res.status(500).send("Belum ada lawan yang masuk ke room anda!");
    return;
  }

  const gameRound = await GameRounds.findOne({
    where: {
      roomId: room.id,
      status: "ACTIVE",
    },
  });

  // ==== Handel apabilan field GameRound belum ada
  if (!gameRound) {
    const numberOfRound = await GameRounds.findAll({
      where: {
        roomId: room.id,
      },
    });

    // == Handel agar tidak bermain melebihi jumlah ronde pada room
    if (numberOfRound.length === room.roundNum) {
      res
        .status(500)
        .send("Permainan sudah berakhir, silahkan membuat/join room baru!");
      return;
    }

    const payload = {
      roomId: room.id,
      status: "ACTIVE",
    };

    const gameRound = await GameRounds.create(payload);

    await GameRoundHistories.create({
      roundId: gameRound.id,
      playerId: req.user.id,
      selected,
    });

    const isRound = await GameRounds.findAll({
      where: { roomId: room.id },
    });

    const response = {
      Round: isRound.length,
      selected,
    };
    res.send(response);
    return;
  }

  const checkUserGameRound = await GameRounds.findOne({
    where: {
      roomId: room.id,
      status: "ACTIVE",
    },
    include: GameRoundHistories,
  });

  //  ==== Handel apabila user sudah memasukkan selected pada round ini
  if (checkUserGameRound) {
    if (checkUserGameRound.GameRoundHistories[0]?.playerId === req.user.id) {
      res.status(500).send("Anda sudah memasukkan pilihan di ronde ini");
      return;
    }
  }

  const gameHistory = await GameRoundHistories.create({
    roundId: room.GameRounds[0].id,
    playerId: req.user.id,
    selected,
  });

  const checkSelectedTwoPlayers = await GameRoundHistories.findAll({
    where: { roundId: gameHistory.roundId },
  });
  const resultWin = checkRoleGame(
    checkSelectedTwoPlayers[0].selected,
    checkSelectedTwoPlayers[1].selected
  );

  let winnerId;
  checkSelectedTwoPlayers.forEach((player) => {
    if (resultWin === player.selected) {
      winnerId = player.playerId;
    }
  });

  await GameRounds.update(
    {
      winnerId: resultWin === "DRAW" ? null : winnerId,
      status: "FINISHED",
    },
    {
      where: {
        roomId: room.id,
        status: "ACTIVE",
      },
    }
  );

  const numberOfRoundAfterPlaying = await GameRounds.findAll({
    where: {
      roomId: room.id,
    },
  });

  if (numberOfRoundAfterPlaying.length === room.roundNum) {
    const calculationGameRound = await GameRounds.findAll({
      where: { roomId: room.id },
    });

    const winnersId = [];
    calculationGameRound.forEach((player) => {
      if (player.winnerId !== null) {
        winnersId.push(player.winnerId);
      }
    });

    const playerWinId = calculation(winnersId);

    Rooms.update(
      {
        winnerId: playerWinId !== null ? playerWinId : "Draw",
        status: "FINISHED",
      },
      {
        where: {
          code,
        },
      }
    );
  }

  res.json({
    winnerId: resultWin === "DRAW" ? "Pertandingan Draw" : winnerId,
    code,
  });
};

const { Users } = require("../../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.login = async (req, res) => {
  // res.send("Login berhasil");
  const { username, password } = req.body;
  const user = await Users.findOne({
    where: { username },
  });

  if (!user) {
    res.status(401).send("User tidak ditemukan");
  } else {
    validPass = await bcrypt.compare(password, user.password);
    if (!validPass) res.status(401).send("Password salah");
    else {
      const { email, username, name, id } = user;
      const token = jwt.sign({ email, username, name, id }, "secret");

      const response = {
        username: user.username,
        accessToken: token,
      };
      res.send(response);
    }
  }
};

exports.register = async (req, res) => {
  try {
    const passwordBcrypt = await bcrypt.hash(req.body.password, 10);
    req.body.password = passwordBcrypt;
    const newUser = await Users.create(req.body);

    const { name, email, username } = newUser;
    const viewUser = { name, email, username };

    res.send(viewUser);
  } catch (err) {
    console.log(err);
    res.status(500).send("Sesuatu Ada yang salah");
  }
};

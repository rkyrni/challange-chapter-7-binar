const calculation = (array) => {
  if (array.length === 0) return null;
  let data1 = array[0];
  let jumlahData1 = 0;
  let data2;
  let jumlahData2 = 0;

  array.forEach((item) => {
    if (item === data1) {
      jumlahData1++;
    }
  });

  array.forEach((item) => {
    if (item !== data1) data2 = item;

    if (item === data2) {
      jumlahData2++;
    }
  });

  if (jumlahData1 > jumlahData2) {
    return data1;
  } else if (jumlahData1 === jumlahData2) {
    return null;
  } else {
    return data2;
  }
};

module.exports = calculation;

const checkRoleGame = (selectedPlayerOne, selectedPlayerTwo) => {
  let resultWin = "";
  if (selectedPlayerOne === "PAPER") {
    if (selectedPlayerTwo === "PAPER") resultWin = "DRAW";
    else if (selectedPlayerTwo === "ROCK") resultWin = "PAPER";
    else if (selectedPlayerTwo === "SCISSOR") resultWin = "SCISSOR";
  } else if (selectedPlayerOne === "ROCK") {
    if (selectedPlayerTwo === "PAPER") resultWin = "PAPER";
    else if (selectedPlayerTwo === "ROCK") resultWin = "DRAW";
    else if (selectedPlayerTwo === "SCISSOR") resultWin = "ROCK";
  } else if (selectedPlayerOne === "SCISSOR") {
    if (selectedPlayerTwo === "PAPER") resultWin = "SCISSOR";
    else if (selectedPlayerTwo === "ROCK") resultWin = "ROCK";
    else if (selectedPlayerTwo === "SCISSOR") resultWin = "DRAW";
  }

  return resultWin;
};

module.exports = checkRoleGame;
